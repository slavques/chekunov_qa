[Примеры тест-кейсов](https://docs.google.com/spreadsheets/d/13-9UoLwCqbkLVEPN_JjQQGcu-WGmKBet/edit?usp=sharing&ouid=100567851449893152281&rtpof=true&sd=true)

---

[Примеры реальных баг-репортов](https://docs.google.com/spreadsheets/d/1VbRBs0bqEg_XMPmja2E8yAsSdwid3vVltvqaT3zzvTw/edit?usp=sharing)

---

[Тестовое задание (TestSuite, TestCase, SmokeTest)](https://docs.google.com/spreadsheets/d/1IiI2CmigpUOxNLW10tw0bs9i_DAS5ZU_W7KLkxIlgw8/edit?usp=sharing)

---

[Тест-план ASOS.com](https://docs.google.com/document/d/18LhVWlKudwYoGGARzl12iZI96G6Uyo8XAu7X1aHRZC8/edit?usp=sharing)

---

[Тест-кейсы "ОЕДА" iOSApp](https://docs.google.com/spreadsheets/d/1WtS_AQk0N55UsP7yAfwzWKGbhEEN4P9QnHbIX--OxTw/edit?usp=sharing)

---

[Тестирование API - https://app.timetta.com (Postman)](https://www.getpostman.com/collections/f4c1d503bf540aa01c63)

---

[Тест-кейсы https://www.tretyakovgallery.ru](https://docs.google.com/spreadsheets/d/1hJaC4Vmmek8v5nQqqnVcTkjX9Tw7-kBJ/edit?usp=sharing&ouid=100567851449893152281&rtpof=true&sd=true)

---

[Техническое задание SQL Java Python от Bell Integrator](https://docs.google.com/document/d/1AuH39V2BJV_wzL3nlvPmLS4jx97nQMwG/edit?usp=sharing&ouid=100567851449893152281&rtpof=true&sd=true)

---

[Тест-кейсы Telegram (функционал отправки текстовых сообщений) от Wazzup](https://docs.google.com/spreadsheets/d/1siQWK9gxbx4U3-RAZo8miqPo7dK17W6SOeLygcyZKpk/edit?usp=sharing)